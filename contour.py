from PIL import Image
from pylab import *

# read image to array
im = array(Image.open('cvijet.jpg').convert('L'))
figure()
contour(im, cmap='gray', origin='image')
