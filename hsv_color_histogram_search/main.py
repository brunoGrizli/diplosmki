from os import walk
from PIL import Image
import hsv_color_histogram_search.dao as Dao
import hsv_color_histogram_search.hsv_helper as HSVHelper
import operator
import histogram_functions as HistogramFunctions
import hsv_color_histogram_search.histogram_helper as ShowHelper

def process_image(path):
    image = Image.open(path)
    if (image.mode == 'L'):
        return
    image_hsv = HSVHelper.convert_to_hsv(image)
    imageData, new_im = HSVHelper.extract_colors(image_hsv, path)
    Dao.save_image_data(imageData)

def process_images():
    k=1
    for (dir, allDirs, files) in walk('images'):
        if (k>30):
            break
        print("importing images from " + dir)
        for file in files:
            path = dir + '/' + file
            process_image(path)
        k+=1

def load_images():
    cursor = Dao.get_all_images_data()
    images = []
    images.extend(cursor[:])
    return images

def find_similar_images(imagePath):
    images = load_images()
    image = Image.open(imagePath)
    if (image.mode == 'L'):
        print("Cant find matches for grayscale images")
        return
    image_hsv = HSVHelper.convert_to_hsv(image)
    image_data, new_im = HSVHelper.extract_colors(image_hsv, imagePath)

    print("Image colors")
    print(image_data)

    dist = {}
    for im in images:
        distance = HistogramFunctions.distance_chisqrt(im["all"]["histogram"], image_data["all"]["histogram"])
        distance += HistogramFunctions.distance_chisqrt(im["all"]["valueHistogram"], image_data["all"]["valueHistogram"])

        distance += HistogramFunctions.distance_chisqrt(im["top"]["histogram"], image_data["top"]["histogram"])
        distance += HistogramFunctions.distance_chisqrt(im["top"]["valueHistogram"], image_data["top"]["valueHistogram"])

        distance += HistogramFunctions.distance_chisqrt(im["middle"]["histogram"], image_data["middle"]["histogram"])
        distance += HistogramFunctions.distance_chisqrt(im["middle"]["valueHistogram"], image_data["middle"]["valueHistogram"])

        distance += HistogramFunctions.distance_chisqrt(im["bottom"]["histogram"], image_data["bottom"]["histogram"])
        distance += HistogramFunctions.distance_chisqrt(im["middle"]["valueHistogram"], image_data["middle"]["valueHistogram"])
        dist[im["path"]] = distance

    # sort
    distSorted = sorted(dist.items(), key=operator.itemgetter(1))
    return distSorted[0:5], image

similar_dict, image = find_similar_images("images/mandolin/image_0011.jpg")
print(similar_dict)
ShowHelper.showResults(similar_dict, image)

#process_images()

