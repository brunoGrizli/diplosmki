from numpy import *
import operator
from matplotlib.pyplot import *
from pathlib import Path
from PIL import Image
import histogram_functions as hf
import hsv_color_histogram_search.hsv_helper as HsvHelper

# Using kNN algorithm find the nearest histograms to imageHistogram
# k represents a number of neighbours to return
def findNearestNeighbours(image_histogram, db_data, k = 5):

    # compute distance to all histograms
    dist = {}
    for imagePath in db_data:
        dist[imagePath] = hf.distance_chisqrt(image_histogram[0], db_data[imagePath])

    # sort
    distSorted = sorted(dist.items(), key=operator.itemgetter(1))
    return distSorted[0:k]

# Display results
def showResults(nearestNeighbours, image):
    print(nearestNeighbours)
    nmbr = (len(nearestNeighbours) + 1) # +1 for origin image, *2 for more space
    k = 0
    fig = figure(1)

    for neighbour in nearestNeighbours:
        imagePath = neighbour[0]
        p = Path(imagePath)
        im = Image.open(p)
        fig.add_subplot(nmbr, 2, k+1).imshow(im)
        fig.add_subplot(nmbr, 2, k+2).hist(HsvHelper.get_hues(im), 255)
        k += 2

    #finaly add origin image
    fig.add_subplot(nmbr, 2, k+1).imshow(image)
    fig.add_subplot(nmbr, 2, k+2).hist(HsvHelper.get_hues(image), 255)
    show()
    return