import pymongo
from pymongo import MongoClient

mongoClient = MongoClient()
db = mongoClient.clusterGrayHistograms

def save_image_data(imageData):
    savedImageData = db.histograms.find_one({"path": imageData["path"]})
    if (savedImageData is None):
        db.histograms.insert_one(imageData)
    else:
        db.histograms.update(savedImageData, imageData)

def get_all_images_data():
    return db["histograms"].find()
