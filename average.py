from PIL import Image
from pylab import *
from pathlib import Path
import numpy as np
import derivatives
import harris_detector

imagePaths = [
    "cvijet.jpg",
    #"images/butterfly/image_0001.jpg",
    #"images/butterfly/image_0002.jpg",
    #"images/butterfly/image_0003.jpg",
    #"images/butterfly/image_0004.jpg"
]

def histEqualization(image):
    hist,bins = np.histogram(image.flatten(), 256, normed=True)
    cdf = np.cumsum(hist)
    cdf = 255 * cdf/cdf[-1]
    im2 = np.interp(image.flatten(), bins[:-1], cdf)
    return im2, cdf

def showImage(image):
    imR = image[0:, 0:, 0]
    imG = image[0:, 0:, 1]
    imB = image[0:, 0:, 2]

    figure("Red")
    subplot(411)
    imshow(imR, cmap='Reds')
    subplot(412)
    hist(imR[0], 255)
    imREq, cdf = histEqualization(imR)
    subplot(413)
    imshow(imREq.reshape(imR.shape), cmap='Reds')
    subplot(414)
    plot(cdf)

    figure("Green")
    subplot(211)
    imshow(imG, cmap='Greens')
    subplot(212)
    hist(imG[0], 255)

    figure("Blue")
    subplot(211)
    imshow(imB, cmap='Blues')
    subplot(212)
    hist(imB[0], 255)

    show()
    redTotal = 0
    blueTotal = 0
    greenTotal = 0
    for pixel in image[0]:
        redTotal += pixel[0]
        blueTotal += pixel[1]
        greenTotal += pixel[2]

    redAvg = redTotal/len(image[0])
    print("Average of image " + str(redAvg) + " total " + str(redTotal))

    blueAvg = blueTotal/len(image[0])
    print("Average blue " + str(blueAvg) + " total " + str(blueTotal))

    greenAvg = greenTotal/len(image[0])
    print("Average green " + str(greenAvg) + " total " + str(greenTotal))

for path in imagePaths:
    p = Path(path)
    image = Image.open(p)
    #showImage(array(image))
    #derivatives.sobel_derivatives(array(image.convert('L')))
    #derivatives.gaussian_blurring(array(image), 2)
    #derivatives.gaussian_blurring(array(image), 5)
    #derivatives.gaussian_blurring(array(image), 10)
    harrisMat = harris_detector.compute_harris_matrix(array(image.convert('L')))
    harris_corners = harris_detector.harris_points(harrisMat)
    harris_detector.plot_harris_points(image, harris_corners)
