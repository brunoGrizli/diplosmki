import PIL as pil
import numpy as numpy
import matplotlib.pyplot as pyplot
from pathlib import Path
import histogram_functions as hf

#p1 = Path("images/airplanes/image_0059.jpg")
#p2 = Path("images/airplanes/image_0038.jpg")

p1 = Path("custom_images/image_0004.jpg")
p2 = Path("custom_images/image_0003.jpg")

image1 = pil.Image.open(p1)
image2 = pil.Image.open(p2)

image1_ar = numpy.array(image1)
image2_ar = numpy.array(image2)

image1_r = image1_ar[0:,0:,0]
image1_g = image1_ar[0:,0:,1]
image1_b = image1_ar[0:,0:,2]

image2_r =  image2_ar[0:,0:,0]
image2_g = image2_ar[0:,0:,1]
image2_b = image2_ar[0:,0:,2]

is_norm = True
hist1_r, bin_edges1_r = numpy.histogram(image1_r, 255, normed=is_norm)
hist2_r, bin_edges2_r = numpy.histogram(image2_r, 255, normed=is_norm)

hist1_b, bin_edges1_b = numpy.histogram(image1_b, 255, normed=is_norm)
hist2_b, bin_edges2_b = numpy.histogram(image2_b, 255, normed=is_norm)

hist1_g, bin_edges1_g = numpy.histogram(image1_g, 255, normed=is_norm)
hist2_g, bin_edges2_g = numpy.histogram(image2_g, 255, normed=is_norm)

distChi_r = hf.distance_chisqrt(hist1_r, hist2_r)
distEuclid_r = hf.distance_euclid(hist1_r, hist2_r)
intersaction_r = hf.intersaction(hist1_r, hist2_r)

distChi_g = hf.distance_chisqrt(hist1_g, hist2_g)
distEuclid_g = hf.distance_euclid(hist1_g, hist2_g)
intersaction_g = hf.intersaction(hist1_g, hist2_g)

distChi_b = hf.distance_chisqrt(hist1_b, hist2_b)
distEuclid_b = hf.distance_euclid(hist1_b, hist2_b)
intersaction_b = hf.intersaction(hist1_b, hist2_b)

print("distance euclid red ", distEuclid_r)
print("distance chi red ", distChi_r)
print("intersection red ", intersaction_r)

print("distance euclid green ", distEuclid_g)
print("distance chi green ", distChi_g)
print("intersection green ", intersaction_g)

print("distance euclid blue ", distEuclid_b)
print("distance chi blue ", distChi_b)
print("intersection blue ", intersaction_b)

print("Total euclid: ", distEuclid_r + distEuclid_b + distEuclid_g)
print("Total chi: ", distChi_r + distChi_b + distChi_g)
print("Total intersection: ", intersaction_r + intersaction_g + intersaction_b)

print("Image 1 size: ", image1_ar.shape)
print("Image 2 size: ", image2_ar.shape)

figure = pyplot.figure("Compare histograms")

figure.add_subplot(2, 4, 1).imshow(image1)
figure.add_subplot(2, 4, 2).bar(bin_edges1_r[:-1], hist1_r)
figure.add_subplot(2, 4, 3).bar(bin_edges1_g[:-1], hist1_g)
figure.add_subplot(2, 4, 4).bar(bin_edges1_b[:-1], hist1_b)

figure.add_subplot(2, 4, 5).imshow(image2)
figure.add_subplot(2, 4, 6).bar(bin_edges2_r[:-1], hist2_r)
figure.add_subplot(2, 4, 7).bar(bin_edges2_g[:-1], hist2_g)
figure.add_subplot(2, 4, 8).bar(bin_edges2_b[:-1], hist2_b)
pyplot.show()
