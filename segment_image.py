from scipy.cluster.vq import *
from scipy.misc import imresize
import numpy as numpy
from PIL import Image
from matplotlib import pyplot

steps = 50 # image is divided in steps*steps region
im = numpy.array(Image.open('images/kangaroo/image_0048.jpg').convert('HSV'))

dx = int (im.shape[0] / steps)
dy = int (im.shape[1] / steps)

# compute color features for each region
features = []
for x in range(steps):
  for y in range(steps):
    R = numpy.mean(im[x*dx:(x+1)*dx,y*dy:(y+1)*dy,0])
    G = numpy.mean(im[x*dx:(x+1)*dx,y*dy:(y+1)*dy,1])
    B = numpy.mean(im[x*dx:(x+1)*dx,y*dy:(y+1)*dy,2])
    features.append([R,G,B])
features = numpy.array(features,'f') # make into array

# cluster
centroids,variance = kmeans(features,3)
code,distance = vq(features,centroids)

# create image with cluster labels
codeim = code.reshape(steps,steps)
codeim = imresize(codeim,im.shape[:2],interp='nearest')

pyplot.figure()
pyplot.imshow(codeim)
pyplot.show()