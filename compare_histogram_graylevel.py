import PIL as pil
import numpy as numpy
import matplotlib.pyplot as pyplot
from pathlib import Path
import histogram_functions as hf

p1 = Path("images/airplanes/image_0004.jpg")
p2 = Path("images/airplanes/image_0120.jpg")

image1 = pil.Image.open(p1).convert('L')
image2 = pil.Image.open(p2).convert('L')

image1Normed_ar = hf.equilze(image1, 255)
image2Normed_ar = hf.equilze(image2, 255)

image_ex_wh = list(filter(lambda x : x < 255, numpy.array(image1).flatten()))

hist1, bin_edges1 = numpy.histogram(image_ex_wh, 255, normed=True)
hist2, bin_edges2 = numpy.histogram(image2, 255, normed=True)

distChi = hf.distance_chisqrt(hist1, hist2)
distEuclid = hf.distance_euclid(hist1, hist2)

print("distance euclid ", distEuclid)
print("distance chi ", distChi)

figure = pyplot.figure("Compare histograms")

figure.add_subplot(4, 2, 1).imshow(image1, cmap="gray")
figure.add_subplot(4, 2, 2).plot(hist1)

figure.add_subplot(4, 2, 5).imshow(image2, cmap="gray")
figure.add_subplot(4, 2, 6).bar(bin_edges2[:-1], hist2)
pyplot.show()