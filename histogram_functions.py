from pylab import *
import numpy as np

def equilze(image, bins=255):
    imageArray = array(image)
    hist = np.histogram(imageArray.flatten(), bins, normed=True)
    cdf = np.cumsum(hist[0])
    cdf = 255 * cdf / cdf[-1]

    imageArrayEq = interp(imageArray.flatten(), hist[1][:-1], cdf)

    return imageArrayEq.reshape(imageArray.shape)

def distance_euclid(hist1, hist2):
    return sqrt(sum(hist1 - hist2) ** 2)

def distance_chisqrt(hist1, hist2):
    result = 0;
    for i in range(0,hist1.__len__()):
        a = hist1[i] - hist2[i]
        b = hist1[i] + hist2[i]
        if (b != 0):
            result += a*a/b
    return result

def intersaction(hist1, hist2):
    ls1 = hist1
    ls2 = hist2
    return sum(ls1 - ls2)

# Calculate image histogram in grayscale with 255 bins
def getImageHistogram(image):
    hsv_image = image.convert('HSV')
    im_array = array(hsv_image)
    hues = im_array[0:, 0:, 0]
    hues = array(hues).flatten()
    return histogram(hues, 255, normed=False)

def getImageHistogramEq(image):
    imageEq =  equilze(image, 255)
    return histogram(imageEq, 255, normed=True)