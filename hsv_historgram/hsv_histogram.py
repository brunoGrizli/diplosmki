import os
import numpy
from PIL import Image


# create a list of images
path = 'images/crocodile'
imlist = [os.path.join(path,f) for f in os.listdir(path) if f.endswith('.jpg')]

# extract feature vector (8 bins per color channel)
features = numpy.zeros([len(imlist), 512])
for i,f in enumerate(imlist):
  im = numpy.array(Image.open(f))

  # multi-dimensional histogram
  h,edges = numpy.histogramdd(im.reshape(-1,3),8,normed=True,
                        range=[(0,255),(0,255),(0,255)])
  features[i] = h.flatten()
