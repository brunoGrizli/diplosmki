from numpy.random import *
from matplotlib.pyplot import  *
from scipy.cluster.vq import *
from numpy import *

colors = ['c', 'r', 'g', 'b', 'y', 'c']
k = 5
class2 = randn(100,2) + array([5,5])
class1 = 1.5 * randn(100,2)
feature = vstack((class1, class2))
centroids, variance = kmeans(feature, k)
code,distance = vq(feature,centroids)
figure()
current = 0
while(current<k):
    ndx = where(code==current)
    plot(feature[ndx, 0], feature[ndx, 1], colors[current]+'.')
    current += 1
show()