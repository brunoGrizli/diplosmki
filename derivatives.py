from numpy import *
from scipy.ndimage import filters
from matplotlib.pyplot import *

def sobel_derivatives(imageArr):
    imx = zeros(imageArr.shape)
    filters.sobel(imageArr, 1, imx)

    imy = zeros(imageArr.shape)
    filters.sobel(imageArr, 0, imy)

    magnitude = sqrt(imx**2 + imy**2)
    magFiltered = array(list(map(lambda x: x if x > 200 else 0, magnitude.flatten())))
    magFiltered = magFiltered.reshape(magnitude.shape)
    show_derivatives(imx, imy, magFiltered)

def gaussian_derivatives(imageArr, sigma=5):
    imx = zeros(imageArr.shape)
    filters.gaussian_filter(imageArr, (sigma, sigma), (0,1), imx)
    imy = zeros(imageArr.shape)
    filters.gaussian_filter(imageArr, (sigma, sigma), (1,0), imy)

    magnitude = sqrt(imx**2 + imy**2)
    show_derivatives(imx, imy, magnitude)

def gaussian_blurring(imageArr, sigma=5):
    imBlurred = zeros(imageArr.shape)
    for i in range(3):
        imBlurred[:,:,i] = filters.gaussian_filter(imageArr[:,:,i], sigma)
    imBlurred = uint8(imBlurred)
    imshow(imBlurred)
    show()

def show_derivatives(imx, imy, magnitude):
    fig = figure()
    gray()
    left = fig.add_subplot(131)
    right = fig.add_subplot(132)
    down = fig.add_subplot(133)

    left.imshow(imx)
    right.imshow(imy)
    down.imshow(magnitude)
    show()



