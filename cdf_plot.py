from PIL import Image
from pylab import *
import numpy as np

image = Image.open('images/butterfly/image_0001.jpg').convert('L')
imageArray = array(image)
print("image size", image.size)
hist = hist(imageArray.flatten(), 255, normed=True)

cdf = np.cumsum(hist[0])
cdf = 255 * cdf / cdf[-1]

im2 = interp(imageArray.flatten(),hist[1][:-1],cdf)
hist2 = np.histogram(im2, 255, normed=True)
fig2 = figure("Second")
fig2.add_subplot(1,1,1).bar(hist2[1][:-1], hist2[0])
imshow(im2.reshape(imageArray.shape), cmap='gray')

print(cdf)
fig1 = figure("First")
imshow(imageArray, cmap='gray')
show()