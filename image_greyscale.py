from PIL import Image
from pylab import *

image = array(Image.open('cvijet.jpg').convert('L'))
figure()
imshow(image, cmap='gray')
show()