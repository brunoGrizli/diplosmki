from scipy.cluster.vq import *
from scipy.misc import imresize
from matplotlib.pyplot import *
from numpy import *
from PIL import Image
from pathlib import Path
from collections import Counter
from pymongo import MongoClient

imagePaths = [
    "cvijet.jpg",
    "images/butterfly/image_0001.jpg",
    "images/butterfly/image_0002.jpg",
    "images/butterfly/image_0003.jpg",
    "images/butterfly/image_0004.jpg",
    "images/anchor/image_0001.jpg",
    "images/anchor/image_0002.jpg",
    "images/anchor/image_0003.jpg",
    "images/anchor/image_0004.jpg",
    "images/camera/image_0001.jpg",
    "images/camera/image_0002.jpg",
    "images/camera/image_0003.jpg",
    "images/camera/image_0004.jpg",
    "images/emu/image_0001.jpg",
    "images/emu/image_0002.jpg",
    "images/emu/image_0003.jpg",
    "images/emu/image_0004.jpg",
    "images/lamp/image_0026.jpg"
]

#all image features
features = []

def get_image_regions(image):
  imArray = array(image)
  dx = int(imArray.shape[0] / 5)
  dy = int(imArray.shape[1] / 5)

  # compute pixel features for each region
  regions = []
  for x in range(5):
    for y in range(5):
      region = imArray[x * dx:(x + 1) * dx, y * dy:(y + 1) * dy]
      regions.append(region)
  return regions

def get_histogram_feature_vector(region):
    hist_feature = histogram(region.flatten(), 255)
    return hist_feature[0]

def get_region_feature_vector(region, dimension = 10):
  dimx = dimension
  dimy = dimension
  dx = int(region.shape[0] / dimension)
  dy = int(region.shape[1] / dimension)
  if (dx < 1):
    dx = 1
    dimx = region.shape[0]
  if (dy < 1):
    dy = 1
    dimy = region.shape[1]

  featureVector = zeros(dimension * dimension)


  # compute pixel features for each region
  for x in range(dimx):
    for y in range(dimy):
      featurePoint = mean(region[x * dx:(x + 1) * dx, y * dy:(y + 1) * dy])
      featureVector[(y*10 + x)] = featurePoint

  return featureVector

def add_image_feature(image):
  global features
  regions = get_image_regions(image) # each image is divieded into 25 regions

  for region in regions:
    features.append(get_region_feature_vector(region)) # each region is scaled to 50*50 array


for path in imagePaths:
    p = Path(path)
    image = Image.open(p).convert("L")
    add_image_feature(image)

# cluster
centroids, variance = kmeans(features, 20)
print(centroids[0])

client = MongoClient()
db = client.diplomski

code, distance = vq(features, centroids)

for i in range(0, len(code), 25):
    ccs = Counter(code[i:i+25])
    print(ccs.__str__())

centroidsFlatten = centroids.flatten()
db.centrois.insert(centroidsFlatten)
db.features.insert(features)
balSize = 100
newSize = int(len(centroidsFlatten)/balSize)

centroidsIm = centroidsFlatten.reshape(newSize,balSize)
codeim = imresize(centroidsIm, centroidsIm.shape,interp='nearest')

imshow(centroidsIm, cmap="gray")
show()