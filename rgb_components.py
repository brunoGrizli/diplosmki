from PIL import Image
from pylab import *
from scipy.cluster.vq import *

# read image to array
im = array(Image.open('cvijet.jpg').convert("HSV"))
imR = im[0:,0:,0]
imG = im[0:,0:,1]
imB = im[0:,0:,2]
figure(1)
imshow(imR, cmap='Reds')
figure(2)
imshow(imG, cmap='Greens')
figure(3)
imshow(imB, cmap='Blues')

show()
vq()